# ft_irc

A 42 school Unix / networking team project.

- kryckely : lead developer
- agoublai : product designer
- wetieven : technical manager

## Goal

Coding a basic IRC server in C++

## Context

IRC (Internet Relay Chat) is an Application Layer communication protocol of the Internet protocol suite.

It is designed to allow instant text messaging between its users using the networking capabilities of the TCP/IP protocol.

Thus, it is a forebearer of all the "convenient", unefficient, centralised, capitalistic, data mined, annoying... instant messaging "apps" you have to use nowadays to stay in touch with less technologically conscious people.

## Resources

As a venerable Internet protocol IRC is very well documented.

- Starting with its original RFC : https://www.rfc-editor.org/rfc/rfc1459.html

- Augmented by this working document incorporating the original RFC amendments and many practical examples to clarify its guidelines : https://modern.ircdocs.horse

We can even check out fully fledged implementation of IRC servers since the most popular one is FOSS.
- Repos of UnrealIRCd : https://github.com/unrealircd/unrealircd

## Interest

Using client/server sockets and dipping a toe in applied OOP.

Not much in the way of design here though, as the IRC protocol doesn't leave much to the imagination !

Still an opportunity to practice proper project architecture with pertinent and legible classes.

## Usage and evaluation

Since we now have to opportunity to do so, we gladly chose to surrender this project for Linux, using Linux's specific (and superior) epoll() fd polling feature over poll() and select().

In order to comply with 42's school rules, their interpretation, and the absence of Linux machines at Lyon's campus, we're surrendering it on a macOS machine using a Linux interactive Docker container of our making : https://github.com/Le-Technologue/debug-tools/tree/ft-irc-netcat-weechat.

Since epoll() isn't carried by the ancient macOS libraries, evaluation process will entirely take place inside that Linux container.

### Interactive docker container

- So first and foremost, launch the debug-tools interactive container thanks to our bash script :
	```
	[path to]/dbg-tools [-b to rebuild] [volume to mount inside home dir]
	```

- Launch another interactive terminal inside the container (to keep an eye on the server output in a dedicated terminal split, and/or connect multiple clients to our server) :
	```
	docker exec -it [container name - in our case : dbg_tools] [entrypoint - in our case : fish]
	```

- Check out debug-tools container repo and documentation for more info.

### Our IRC server

- Once you're inside the container's prompt, compile the server :
	```
	make
	``` 

- Launch the server :
	```
	./ircserv <port> <password>
	```

- If you keep it in your terminal foreground, you'll witness its log and error messages.

### Netcat

- Netcat is a barebones TCP/IP interactive network interface that allows you to connect manually to our server like so :
	```
	nc <ip address - in our case 127.0.0.1> <port>
	```

- You can then test the IRC connection protocol by manually inputing the following IRC commands inside netcat's prompt :
	```
	PASS ******
	NICK nickname
	USER username * * :realname
	```

- You can even create a channel (by being the first to join it, as per IRC's protocols) :
	```
	JOIN #firstchan
	```

### Weechat

Weechat is our reference client of choice for the evaluation, our server implementation is attuned to its behavior.

- Launch weechat (installed through Debian repositories in our container) specifying a dedicated log and configuration directory (so we can have several weechat instances running simultaneously on our machine):

	```
	weechat --dir weechat-logs
	```

- Add our server to weechat's server list :

	```
	/server add <server name> <address>/<port>
	```

	- In our case :
	```
	/server add ft_irc 127.0.0.1/8080
	```
	
- Connect to the server :

	```
	/connect <server name> -password=****
	```
	
	- In our case :
	```
	/connect ft_irc -password=****
	```
	
- Join a channel :

	```
	/join #chan
	```

- Open another weechat instance from a different terminal, using its own dedicated log/conf directory :

	```
	weechat --dir weechat-logs2
	```
