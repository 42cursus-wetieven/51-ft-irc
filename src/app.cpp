#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/socket.h>

#include <Client.hpp>
#include <Server.hpp>
#include <csignal>
#include <iostream>

static Server* serv = NULL;

void signal_handler(int signal)
{
    (void)signal;
    serv->Destroy();
    delete (serv);
}

int main(int argc, char* argv[])
{
    std::signal(SIGINT, signal_handler);
    if (argc != 3) {
        std::cout << "Usage: ./ircserv <port> <password>" << std::endl;
        exit(EXIT_FAILURE);
    }

    /** Init Server */
    Server* server = new Server(argv[1], argv[2]);
    serv = server;
    const int fdsocket = server->getFdSocket();
    (void)fdsocket;
    /** Init Server */

    server->run();
    delete server;

    return (EXIT_SUCCESS);
}
