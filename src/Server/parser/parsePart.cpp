#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::PartClientFromChannels(std::vector<std::string> neededChannels, Client* client)
{
    for (size_t i_neededChannels = 0; i_neededChannels < neededChannels.size(); i_neededChannels++) {
        if (IsCorrectChannelName(neededChannels[i_neededChannels]) == false) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(neededChannels[i_neededChannels]));
            return (failure);
        }
        Channel* found = SearchChannelFrom(neededChannels[i_neededChannels], _channels);
        if (found == NULL) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(neededChannels[i_neededChannels]));
            return (failure);
        }
        if (found->IsThisClientConnected(client) == false) {
            SendMessageTo(client, ERR_NOTONCHANNEL(neededChannels[i_neededChannels]));
            return (failure);
        } else {
            SendMessageTo(found->GetClients(), RPL_PART(client->GetNickname(), neededChannels[i_neededChannels]));
            ClientLeave(found, client);
        }
    }
    return (success);
}

Result Server::ParsePartWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    if (args.size() < 1) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("PART"));
        return (failure);
    }
    const std::vector<std::string> string_channels = ExtractValuesBetweenCommas(args[0]);
    if (PartClientFromChannels(string_channels, client) == failure)
        return (failure);

    return (success);
}
