#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::ParseInviteWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    else if (args.size() < 2) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("INVITE"));
        return (failure);
    }
    if (IsCorrectChannelName(args[1])) {
        Channel* channel_found = SearchChannelFrom(args[1], _channels);
        if (channel_found == NULL) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(args[1]));
            return (failure);
        } else if (channel_found->IsThisClientConnected(client) == false) {
            SendMessageTo(client, ERR_NOTONCHANNEL(args[1]));
            return (failure);
        }
        if (IsCorrectClientNickname(args[0])) {
            Client* client_found = SearchClientFrom(args[0], _clients);
            if (client_found == NULL) {
                SendMessageTo(client, ERR_NOSUCHNICK(args[0]));
                return (failure);
            } else if (channel_found->IsThisClientConnected(client_found) == true) {
                SendMessageTo(client, ERR_USERONCHANNEL(args[0], args[1]));
                return (failure);
            }
            if (channel_found->GetIsInviteOnly() == true) {
                if (channel_found->IsThisClientOperator(client) == false) {
                    SendMessageTo(client, ERR_CHANOPRIVSNEEDED(args[1]));
                    return (failure);
                }
            }
            channel_found->AddClient(client_found);
            client_found->AddJoinedChannelBy(channel_found->GetId());
            SendMessageTo(client, RPL_INVITING(client->GetNickname(), client_found->GetNickname(), "#" + channel_found->GetName()));
            SendMessageTo(client_found, RPL_INVITED(client->GetNickname(), client_found->GetNickname(), "#" + channel_found->GetName()));
            SendMessageTo(channel_found->GetClients(), RPL_JOIN(client_found->GetNickname(), "#" + channel_found->GetName()));
            SendMessageTo(client_found, RPL_NAMREPLY(client->GetNickname(), "#" + channel_found->GetName(), GetStringOfConnectedClientsInThis(channel_found)));
            SendMessageTo(client_found, RPL_ENDOFNAMES(client->GetNickname(), "#" + channel_found->GetName()));
            std::string topic = channel_found->GetTopic();
            if (topic.empty())
                SendMessageTo(client_found, RPL_NOTOPIC(client_found->GetNickname(), "#" + channel_found->GetName()));
            else
                SendMessageTo(client_found, RPL_TOPIC(client_found->GetNickname(), "#" + channel_found->GetName(), topic));
        }
    }
    return (success);
}
