#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::ParseOperWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    if (args.size() < 2) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("OPER"));
        return (failure);
    }
    if (args[0].compare(_userOperator)) {
        SendMessageTo(client, ERR_NOOPERHOST);
        return (failure);
    } else if (args[1].compare(_passwordOperator)) {
        SendMessageTo(client, ERR_PASSWDMISMATCH);
        return (failure);
    }
    SendMessageTo(client, RPL_YOUREOPER(client->GetNickname()));
    client->SetIsServerOperator(true);

    return (success);
}
