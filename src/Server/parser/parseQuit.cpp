#include <Server.hpp>
#include <returnMessages.hpp>

#include <unistd.h>

Result Server::ParseQuitWith(const std::vector<std::string> args, Client* client)
{
    if (args.size() < 1)
        ClientDestroy(client, RPL_QUIT(client->GetNickname(), "Client"));
    else
        ClientDestroy(client, RPL_QUIT(client->GetNickname(), args[0]));

    return (success);
}
