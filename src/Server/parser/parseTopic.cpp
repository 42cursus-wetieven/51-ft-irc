#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::ParseTopicWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    else if (args.size() < 1) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("TOPIC"));
        return (failure);
    }
    if (IsCorrectChannelName(args[0])) {
        Channel* found = SearchChannelFrom(args[0], _channels);
        if (found == NULL) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(args[0]));
            return (failure);
        } else if (found->IsThisClientConnected(client) == false) {
            SendMessageTo(client, ERR_NOTONCHANNEL(args[0]));
            return (failure);
        }
        if (args.size() == 1) {
            std::string topic = found->GetTopic();
            if (topic.empty())
                SendMessageTo(client, RPL_NOTOPIC(client->GetNickname(), args[0]));
            else
                SendMessageTo(client, RPL_TOPIC(client->GetNickname(), args[0], topic));
        } else {
            if (found->IsThisClientOperator(client) == false) {
                SendMessageTo(client, ERR_CHANOPRIVSNEEDED(args[0]));
                return (failure);
            } else {
                found->SetTopic(args[1]);
                SendMessageTo(found->GetClients(), RPL_TOPIC(client->GetNickname(), args[0], args[1]));
            }
        }
    } else {
        SendMessageTo(client, ERR_NOSUCHCHANNEL(args[0]));
        return (failure);
    }
    return (success);
}
