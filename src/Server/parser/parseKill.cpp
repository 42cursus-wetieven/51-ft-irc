#include <Server.hpp>
#include <returnMessages.hpp>

#include <unistd.h>

Result Server::ParseKillWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    if (args.size() < 2) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("KILL"));
        return (failure);
    } else if (client->GetIsServerOperator() == false) {
        SendMessageTo(client, ERR_NOPRIVILEGES);
        return (failure);
    }
    if (IsCorrectClientNickname(args[0])) {
        Client* found = SearchClientFrom(args[0], _clients);
        if (found == NULL) {
            SendMessageTo(client, ERR_NOSUCHNICK(args[0]));
            return (failure);
        }
        ClientDestroy(found, RPL_QUIT(found->GetNickname(), "Client"));
    } else {
        SendMessageTo(client, ERR_NOSUCHNICK(args[0]));
        return (failure);
    }

    return (success);
}
