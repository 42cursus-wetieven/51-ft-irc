#include <Server.hpp>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

static std::vector<std::string> build_vector_keyword(void)
{
    // { "PASS", "NICK", "USER", "PRIVMSG", "JOIN", "PART", "MODE", "QUIT", "INVITE", "KICK", "TOPIC", "OPER", "KILL", "PING" };
    std::vector<std::string> result;
    result.push_back("PASS");
    result.push_back("NICK");
    result.push_back("USER");
    result.push_back("PRIVMSG");
    result.push_back("JOIN");
    result.push_back("PART");
    result.push_back("MODE");
    result.push_back("QUIT");
    result.push_back("INVITE");
    result.push_back("KICK");
    result.push_back("TOPIC");
    result.push_back("OPER");
    result.push_back("KILL");
    result.push_back("PING");
    result.push_back("NOTICE");
    return (result);
}

Result Server::IdentifyKeywordIn(const std::string message)
{
    static std::vector<std::string> keywordMapStrings = build_vector_keyword();
    std::string keyword;

    std::stringstream message_stream(message);
    if (!(message_stream >> keyword)) {
        std::cout << "Pas de keyword" << std::endl;
        return (failure);
    }

    std::vector<std::string>::iterator it = std::find(keywordMapStrings.begin(), keywordMapStrings.end(), keyword);
    if (it == keywordMapStrings.end()) {
        // std::cout << "keyword incorrect" << std::endl;
        return (failure);
    } else {
        // std::cout << "keyword found" << std::endl;
        _keywordValue = static_cast<Server::keyword_values>(it - keywordMapStrings.begin());
    }
    return (success);
}

static std::string StringToLower(std::string strToConvert)
{
    std::string::iterator p;
    for (p = strToConvert.begin(); strToConvert.end() != p; ++p)
        *p = std::tolower(*p);

    return (strToConvert);
}

const std::vector<std::string> Server::ExtractArgsFrom(const std::string message) const
{
    std::string value;
    std::vector<std::string> vec;

    std::stringstream message_stream(message);
    message_stream >> value;

    while (message_stream >> value) {
        if (value[0] == ':') {
            std::string special_string = value.substr(1);
            while (message_stream >> value) {
                // if (value[value.size()] == ':') {
                //     value.pop_back();
                //     special_string += ' ' + value;
                //     break;
                // } else {
                special_string += ' ' + value;
                // }
            }
            vec.push_back(special_string);
        } else {
            if (value[0] == '#') {
                value = StringToLower(value);
            }
            vec.push_back(value);
        }
    }

    return (vec);
}

const std::vector<std::string> Server::ExtractValuesBetweenCommas(const std::string arg) const
{
    std::vector<std::string> vec;
    std::stringstream arg_stream(arg);
    while (arg_stream.good()) {
        std::string substr;
        getline(arg_stream, substr, ',');
        vec.push_back(substr);
    }
    return (vec);
}

Result Server::ParseMessage(const std::string message, Client* client)
{
    static Result (Server::*Parser[])(const std::vector<std::string> args, Client* client) = { &Server::ParsePassWith, &Server::ParseNickWith, &Server::ParseUserWith, &Server::ParsePrivmsgWith, &Server::ParseJoinWith, &Server::ParsePartWith, &Server::ParseModeWith, &Server::ParseQuitWith, &Server::ParseInviteWith, &Server::ParseKickWith, &Server::ParseTopicWith, &Server::ParseOperWith, &Server::ParseKillWith, &Server::ParsePingWith, &Server::ParseNoticeWith };

    if (IdentifyKeywordIn(message) == failure)
        return (failure);
    const std::vector<std::string> args = ExtractArgsFrom(message);
    Result (Server::*SelectedParser)(const std::vector<std::string> args, Client* client) = Parser[_keywordValue];
    if ((this->*SelectedParser)(args, client) == failure)
        return (failure);
    return (success);

    //     clientToSend->SetIsPasswordOk(this->Authenticate(str_buffer, clientToSend));
}
