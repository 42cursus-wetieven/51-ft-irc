#include <Server.hpp>
#include <returnMessages.hpp>

#include <Channel.hpp>
#include <iostream>
#include <string>
#include <vector>

Result Server::JoinChannelsFrom(std::vector<std::string> neededChannels, Client* client)
{
    for (size_t i_neededChannels = 0; i_neededChannels < neededChannels.size(); i_neededChannels++) {
        if (IsCorrectChannelName(neededChannels[i_neededChannels]) == false) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(neededChannels[i_neededChannels]));
            return (failure);
        }
        Channel* found = SearchChannelFrom(neededChannels[i_neededChannels], _channels);
        if (found == NULL)
            AddChannel(neededChannels[i_neededChannels], client);
        else {
            if (found->GetIsInviteOnly() == false) {
                found->AddClient(client);
                client->AddJoinedChannelBy(found->GetId());
                std::string topic = found->GetTopic();
                SendMessageTo(found->GetClients(), RPL_JOIN(client->GetNickname(), neededChannels[i_neededChannels]));
                SendMessageTo(client, RPL_NAMREPLY(client->GetNickname(), neededChannels[i_neededChannels], GetStringOfConnectedClientsInThis(found)));
                SendMessageTo(client, RPL_ENDOFNAMES(client->GetNickname(), neededChannels[i_neededChannels]));
                if (topic.empty())
                    SendMessageTo(client, RPL_NOTOPIC(client->GetNickname(), neededChannels[i_neededChannels]));
                else
                    SendMessageTo(client, RPL_TOPIC(client->GetNickname(), neededChannels[i_neededChannels], topic));
            } else {
                SendMessageTo(client, ERR_INVITEONLYCHAN(neededChannels[i_neededChannels]));
                return (failure);
            }
        }
    }
    return (success);
}

Result Server::ParseJoinWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    if (args.size() < 1) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("JOIN"));
        return (failure);
    }
    const std::vector<std::string> string_channels = ExtractValuesBetweenCommas(args[0]);
    if (JoinChannelsFrom(string_channels, client) == failure)
        return (failure);
    // SendMessageTo(recipients, args[1]);

    return (success);
}
