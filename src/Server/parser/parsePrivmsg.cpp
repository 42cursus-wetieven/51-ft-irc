#include <Server.hpp>
#include <returnMessages.hpp>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

void Server::SendMessageFrom(std::vector<Client*>& result, Client* value, Client* sender, const std::string recipient, const std::string message)
{
    if (std::find(result.begin(), result.end(), value) == result.end()) {
        if (value != sender) {
            result.push_back(value);
            if (value->GetIsRegistered() == true)
                SendMessageTo(value, RPL_AWAY(sender->GetNickname(), recipient, message));
        }
    }
}

Result Server::SearchClientsFromNickname(std::vector<std::string> neededClients, std::vector<Client*>& result, Client* sender, const std::string message)
{
    for (size_t i_neededClients = 0; i_neededClients < neededClients.size(); i_neededClients++) {
        if (IsCorrectChannelName(neededClients[i_neededClients])) {
            Channel* found = SearchChannelFrom(neededClients[i_neededClients], _channels);
            if (found == NULL) {
                SendMessageTo(sender, ERR_NOSUCHNICK(neededClients[i_neededClients]));
                return (failure);
            }
            if (found->IsThisClientConnected(sender) == false) {
                SendMessageTo(sender, ERR_CANNOTSENDTOCHAN(sender->GetNickname(), neededClients[i_neededClients]));
                return (failure);
            }
            const std::vector<Client*> clientsOnChannel = found->GetClients();
            for (size_t i_clientsOnChannel = 0; i_clientsOnChannel < clientsOnChannel.size(); i_clientsOnChannel++) {
                SendMessageFrom(result, clientsOnChannel[i_clientsOnChannel], sender, neededClients[i_neededClients], message);
            }

        } else if (IsCorrectClientNickname(neededClients[i_neededClients])) {
            for (size_t i_availableClients = 0; i_availableClients < _clients.size(); i_availableClients++) {
                if (_clients[i_availableClients]->GetNickname() == neededClients[i_neededClients]) {
                    SendMessageFrom(result, _clients[i_availableClients], sender, neededClients[i_neededClients], message);
                    break;
                } else if (i_availableClients == _clients.size() - 1) {
                    SendMessageTo(sender, ERR_NOSUCHNICK(neededClients[i_neededClients]));
                    return (failure);
                }
            }
        } else {
            SendMessageTo(sender, ERR_NOSUCHNICK(neededClients[i_neededClients]));
            return (failure);
        }
    }
    return (success);
}

Result Server::ParsePrivmsgWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    if (args.size() < 1) {
        SendMessageTo(client, ERR_NORECIPIENT);
        return (failure);
    } else if (args.size() < 2) {
        SendMessageTo(client, ERR_NOTEXTTOSEND);
        return (failure);
    }
    const std::vector<std::string> string_recipients = ExtractValuesBetweenCommas(args[0]);
    std::vector<Client*> recipients;
    if (SearchClientsFromNickname(string_recipients, recipients, client, args[1]) == failure)
        return (failure);

    return (success);
}
