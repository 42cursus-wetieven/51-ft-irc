#include <Server.hpp>
#include <returnMessages.hpp>

#include <cstring>

Result Server::ParseNickWith(const std::vector<std::string> args, Client* client)
{
    if (args.size() < 1) {
        SendMessageTo(client, ERR_NONICKNAMEGIVEN);
        return (failure);
    } else if (!IsCorrectClientNickname(args[0])) {
        SendMessageTo(client, ERR_ERRONEUSNICKNAME(args[0]));
        return (failure);
    } else if (SearchClientFrom(args[0], _clients) != NULL) {
        SendMessageTo(client, ERR_NICKNAMEINUSE(args[0]));
        return (failure);
    }
    if (client->CheckIfClientIsRegistered()) {
        SendMessageTo(client, RPL_NICK(client->GetNickname(), args[0]));
        return (success);
    }
    client->SetNickname(args[0]);
    client->SetIsNicknameOk(true);
    if (client->CheckIfClientIsRegistered())
        SendRegisteredReply(client);

    return (success);
}
