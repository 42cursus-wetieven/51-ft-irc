#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::ParsePassWith(const std::vector<std::string> args, Client* client)
{
    if (args.size() < 1) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("PASS"));
        return (failure);
    } else if (args[0].compare(_password)) {
        SendMessageTo(client, ERR_PASSWDMISMATCH);
        return (failure);
    } else if (client->GetIsRegistered()) {
        SendMessageTo(client, ERR_ALREADYREGISTRED);
        return (failure);
    }
    client->SetIsPasswordOk(true);
    if (client->CheckIfClientIsRegistered())
        SendRegisteredReply(client);

    return (success);
}
