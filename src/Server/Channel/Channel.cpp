#include <Channel.hpp>

#include <iostream>

Channel::Channel(const std::string name, const uint id)
    : _id(id)
    , _name(name)
    , _topic("")
    , _isInviteOnly(false)
{
    std::cout << "{debug} (Channel::Channel) Constructor called" << std::endl;
}

Channel::~Channel(void)
{
    std::cout << "{debug} (Channel::Channel) Destructor called" << std::endl;
}
