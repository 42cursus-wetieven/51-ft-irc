#include "Server.hpp"
#include <returnMessages.hpp>

#include <algorithm>
#include <iostream>
#include <iterator>

Channel* Server::SearchChannelFrom(const std::string name, const std::vector<Channel*> channels)
{
    std::string name_of_channel_to_search = name.substr(1);
    for (size_t i = 0; i < channels.size(); i++) {
        if (channels[i]->GetName() == name_of_channel_to_search)
            return (channels[i]);
    }
    return (NULL);
}

Channel* Server::SearchChannelFrom(const uint id, const std::vector<Channel*> channels)
{
    for (size_t i = 0; i < channels.size(); i++) {
        if (channels[i]->GetId() == id)
            return (channels[i]);
    }
    return (NULL);
}

void Server::AddChannel(const std::string name, Client* client)
{
    _channelsIds++;
    std::string channel_name_to_create = name.substr(1);
    Channel* channel = new Channel(channel_name_to_create, _channelsIds);
    _channels.push_back(channel);
    channel->AddClient(client);
    channel->AddOperator(client);
    client->AddJoinedChannelBy(_channelsIds);
    SendMessageTo(client, RPL_JOIN(client->GetNickname(), name));
    SendMessageTo(client, RPL_NAMREPLY(client->GetNickname(), name, GetStringOfConnectedClientsInThis(channel)));
    SendMessageTo(client, RPL_ENDOFNAMES(client->GetNickname(), name));
    SendMessageTo(client, RPL_NOTOPIC(client->GetNickname(), name));
}

bool Server::IsCorrectChannelName(const std::string name)
{
    if (name.size() < 2 || name[0] != '#') {
        return (false);
    }
    return (true);
}

void Server::ClientLeave(Channel* channel, Client* client)
{
    if (channel == NULL)
        return;
    client->DeleteChannelBy(channel->GetId());
    channel->DeleteClient(client);
    if (channel->GetClients().size() < 1) {
        std::vector<Channel*>::iterator it;
        if ((it = std::find(_channels.begin(), _channels.end(), channel)) != _channels.end())
            _channels.erase(it);
        delete channel;
    }
}

std::string Server::GetStringOfConnectedClientsInThis(Channel* channel)
{
    std::vector<Client*> clients = channel->GetClients();
    std::vector<Client*> operators = channel->GetOperators();
    std::string result;

    for (size_t i = 0; i < clients.size(); i++) {
        for (size_t j = 0; j < operators.size(); j++) {
            if (operators[j] == clients[i])
                result += "@";
        }
        result += clients[i]->GetNickname();
        result += " ";
    }
    return (result);
}
