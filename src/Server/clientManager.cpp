#include <Server.hpp>
#include <returnMessages.hpp>

#include <algorithm>
#include <iostream>
#include <unistd.h>

int Server::ReceiveClient(Client* client, char* buffer)
{
    int len = recv(client->GetClientSocket(), buffer, BUFFER_LEN, MSG_DONTWAIT);
    return (len);
}

Client* Server::SearchClientFromFdIn(std::vector<Client*> clients, int fd)
{
    for (size_t i = 0; i < clients.size(); i++) {
        if (clients[i]->GetClientSocket() == fd)
            return (clients[i]);
    }
    return (NULL);
}

void Server::SendMessageTo(Client* client, const std::string message)
{
    send(client->GetClientSocket(), message.c_str(), message.size(), MSG_DONTWAIT);
    std::cout << "{debug} message to " << client->GetNickname() << " >" << message << "^" << std::endl;
}

void Server::SendMessageTo(std::vector<Client*> recipients, const std::string message)
{
    for (size_t i = 0; i < recipients.size(); i++) {
        if (recipients[i]->GetIsRegistered())
            send(recipients[i]->GetClientSocket(), message.c_str(), message.size(), MSG_DONTWAIT);
    }
    std::cout << "{debug} message to channel "
              << " >" << message << "^" << std::endl;
}

Client* Server::SearchClientFrom(const std::string name, const std::vector<Client*> clients)
{
    for (size_t i = 0; i < clients.size(); i++) {
        if (clients[i]->GetNickname() == name)
            return (clients[i]);
    }
    return (NULL);
}

void Server::AddClient(void)
{
    Client* client = new Client(_fdSocket);
    _clients.push_back(client);
}

bool Server::IsCorrectClientNickname(const std::string name)
{
    if (name.empty() || name.size() > 9 || name[0] == '#' || name[0] == '@' || name[0] == '&') {
        return (false);
    }
    return (true);
}

void Server::ClientDestroy(Client* client, const std::string reason)
{
    SendMessageTo(_clients, reason);
    std::vector<uint>
        channels_id = client->GetJoinedChannelsIds();
    for (size_t i = 0; i < channels_id.size(); i++) {
        ClientLeave(SearchChannelFrom(channels_id[i], _channels), client);
    }
    close(client->GetClientSocket());
    std::vector<Client*>::iterator it;
    if ((it = std::find(_clients.begin(), _clients.end(), client)) != _clients.end())
        _clients.erase(it);
    delete client;
}
