#include "Server.hpp"
#include "returnMessages.hpp"

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <iostream>

// Taille de la file d'attente
#define BACKLOG 3

Server::Server(char portString[], std::string password)
    : _port(atoi(portString))
    , _password(password)
    , _userOperator("kryckely")
    , _passwordOperator("JeSuisOperator")
    , _channelsIds(0)
{
    if ((_fdSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
        std::cerr << "Echéc de la création: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    int opt = 1;
    if (setsockopt(_fdSocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) != 0) {
        std::cerr << "Echéc de paramètrage: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    _address.sin_family = AF_INET;
    // Ecoute sur toutes les adresses (INADDR_ANY <=> 0.0.0.0)
    _address.sin_addr.s_addr = INADDR_ANY;
    // Conversion du port en valeur réseaux (Host TO Network Short)
    _address.sin_port = htons(8080);

    if (bind(_fdSocket, (struct sockaddr*)&_address, sizeof(_address)) != 0) {
        std::cerr << "Echéc d'attachement: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    if (listen(_fdSocket, BACKLOG) != 0) {
        std::cerr << "Echéc de la mise en écoute: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
}

Server::~Server(void)
{
}

void Server::SendRegisteredReply(Client* client)
{
    SendMessageTo(client, RPL_WELCOME(client->GetNickname()));
    SendMessageTo(client, RPL_YOURHOST);
    SendMessageTo(client, RPL_CREATED);
    SendMessageTo(client, RPL_MYINFO(client->GetNickname()));
}

int Server::getFdSocket(void) const
{
    return (_fdSocket);
}

void Server::Destroy(void)
{
    for (size_t i = 0; i < _clients.size(); i++) {
        ClientDestroy(_clients[i], RPL_QUIT(_clients[i]->GetNickname(), "Client"));
    }
}
