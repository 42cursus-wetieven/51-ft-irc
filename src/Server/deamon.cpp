#include <Server.hpp>
#include <returnMessages.hpp>

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>

void Server::run(void)
{
    /* Code to set up listening socket, 'fdsocket',
              (socket(), bind(), listen()) omitted */

    _epollFd = epoll_create1(0);
    if (_epollFd == -1) {
        std::cerr << "epoll_create1" << std::endl;
        exit(EXIT_FAILURE);
    }

    _ev.events = EPOLLIN;
    _ev.data.fd = _fdSocket;
    if (epoll_ctl(_epollFd, EPOLL_CTL_ADD, _fdSocket, &_ev) == -1) {
        std::cerr << "epoll_ctl: fdsocket" << std::endl;
        exit(EXIT_FAILURE);
    }

    while (true) {
        _fdCount = epoll_wait(_epollFd, _events, MAX_EVENTS, -1);
        if (_fdCount == -1) {
            std::cerr << "epoll_wait" << std::endl;
            exit(EXIT_FAILURE);
        }

        // Client* client;
        for (int n = 0; n < _fdCount; ++n) {
            if (_events[n].data.fd == _fdSocket) {
                // client = new Client(_fdSocket);
                AddClient();
                _ev.events = EPOLLIN | EPOLLET | EPOLLRDHUP;
                _ev.data.fd = _clients.back()->GetClientSocket();
                if (epoll_ctl(_epollFd, EPOLL_CTL_ADD, _clients.back()->GetClientSocket(), &_ev) == -1) {
                    std::cerr << "epoll_ctl: conn_sock" << std::endl;
                    exit(EXIT_FAILURE);
                }
            } else if (_events[n].events & EPOLLRDHUP) {
                Client* clientToSend = SearchClientFromFdIn(_clients, _events[n].data.fd);
                if (clientToSend->GetBuffer() == "") {
                    ClientDestroy(clientToSend, RPL_QUIT(clientToSend->GetNickname(), "Client"));
                }
            } else {
                Client* clientToSend = SearchClientFromFdIn(_clients, _events[n].data.fd);
                char buffer[BUFFER_LEN] = { 0 };
                int len = ReceiveClient(clientToSend, buffer);
                if (len == -1 && errno != EAGAIN) {
                    // Une erreur est survenue
                    std::cout << "{error} Error at client receive" << std::endl;
                } else if (len == 0) {
                    // Le client s'est déconnecté (extrémité de la socket fermée)
                } else if (len > 0) {
                    // Le client à envoyé des données
                    std::cout << "{debug} Received data (" << len << " bytes) " << buffer << std::endl;
                    clientToSend->ConcatenateBufferWith(buffer);
                    while (clientToSend->DoTheBufferContainACompleteLine() == true) {
                        const std::string clientBuffer = clientToSend->GetAndClearTheBuffer();
                        if (ParseMessage(clientBuffer, clientToSend) == failure) {
                        }
                    }
                }
            }
        }
    }
}
