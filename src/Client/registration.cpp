#include <Client.hpp>

#include <iostream>

bool Client::CheckIfClientIsRegistered(void)
{
    if (_isPasswordOk && _isNicknameOk && _isUserOk) {
        _isRegistered = true;
        std::cout << "{info} Client: '" << _nickname << "' is registered !" << std::endl;
        return (true);
    }
    return (false);
}

/**
 * Setters
 */
void Client::SetIsPasswordOk(const bool set)
{
    _isPasswordOk = set;
}

void Client::SetIsUserOk(const bool set)
{
    _isUserOk = set;
}

void Client::SetIsNicknameOk(const bool set)
{
    _isNicknameOk = set;
}

void Client::SetNickname(const std::string nickname)
{
    _nickname = nickname;
}

void Client::SetUser(const std::string& username, const std::string& host, const std::string& serverName, const std::string& realName)
{
    _user.username = username;
    _user.host = host;
    _user.serverName = serverName;
    _user.realName = realName;
}

void Client::SetIsServerOperator(const bool set)
{
    _isServerOperator = set;
}

/**
 * Getters
 */
bool Client::GetIsPasswordOk(void) const
{
    return (_isPasswordOk);
}

bool Client::GetIsRegistered(void) const
{
    return (_isRegistered);
}

const std::string Client::GetNickname(void) const
{
    return (_nickname);
}

bool Client::GetIsServerOperator(void) const
{
    return (_isServerOperator);
}
