#include "Client.hpp"

#include <algorithm>

void Client::AddJoinedChannelBy(const uint id)
{
    _joinedChannelsIds.push_back(id);
}

void Client::DeleteChannelBy(const uint id)
{
    std::vector<uint>::iterator it;
    if ((it = std::find(_joinedChannelsIds.begin(), _joinedChannelsIds.end(), id)) == _joinedChannelsIds.end())
        _joinedChannelsIds.erase(it);
}

std::vector<uint> Client::GetJoinedChannelsIds(void)
{
    return (_joinedChannelsIds);
}
