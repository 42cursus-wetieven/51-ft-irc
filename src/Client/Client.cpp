#include <Client.hpp>

#include <iostream>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <string>
#include <sys/epoll.h>
#include <sys/socket.h>

Client::Client(const int fdSocket)
    : _fdSocket(fdSocket)
    , _addrLen(sizeof(_clientAddress))
    , _buffer("")
    , _isPasswordOk(false)
    , _isRegistered(false)
    , _isUserOk(false)
    , _isNicknameOk(false)
    , _nickname("")
    , _isServerOperator(false)
{
    std::cout << "{debug} (Client::Client) Constructor called" << std::endl;
    this->SetClientSocket(accept(fdSocket, (struct sockaddr*)this->GetClientAddressAddr(), this->GetAddrLenAddr()));
    this->SetIpFrom_clientAddress();
    std::cout << "{info} Connexion de " << this->GetIp() << ":" << this->GetClientAddressAddr()->sin_port << std::endl;

    // Passage en mode non bloquant
    fcntl(fdSocket, F_SETFL, O_NONBLOCK);
}

Client::~Client(void)
{
    std::cout << "{debug} (Client::Client) Destructor called" << std::endl;
}
