#include <Client.hpp>

#include <iostream>

void Client::SetClientSocket(int clientSocket)
{
    _clientSocket = clientSocket;
    return;
}

int Client::GetClientSocket(void) const
{
    return (_clientSocket);
}

struct sockaddr_in* Client::GetClientAddressAddr(void)
{
    return (&_clientAddress);
}

uint* Client::GetAddrLenAddr(void)
{
    return (&_addrLen);
}

void Client::SetIpFrom_clientAddress(void)
{
    _ip = inet_ntoa(_clientAddress.sin_addr);
    return;
}

char* Client::GetIp(void)
{
    return (_ip);
}

void Client::ConcatenateBufferWith(const std::string buffer)
{
    _buffer += buffer;
}

bool Client::DoTheBufferContainACompleteLine()
{
    if (_buffer.empty())
        return (false);
    size_t found
        = _buffer.find('\n');
    if (found != std::string::npos)
        return (true);
    return (false);
}

const std::string Client::GetAndClearTheBuffer(void)
{
    size_t found = _buffer.find('\n');
    std::string result = _buffer.substr(0, found);
    _buffer = _buffer.substr(found + 1);
    return (result);
}

const std::string Client::GetBuffer(void)
{
    return (_buffer);
}
