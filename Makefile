.DEFAULT_TARGET := all

# PROJECT #

NAME := ircserv
SRC_DIRS ?= src
BUILD_DIRS ?= build
CC = g++

# SRCS_NAMES := apps/app.cpp
# SRCS = $(SRCS_NAMES:%=$(SRC_DIRS)/%.cpp)
SRCS := $(shell find $(SRC_DIRS) -name *.cpp)

OBJS := $(SRCS:$(SRC_DIRS)/%.cpp=$(BUILD_DIRS)/objs/%.o)
DEPS := $(SRCS:$(SRC_DIRS)/%.cpp=$(BUILD_DIRS)/deps/%.d)

CFLAGS := -Wall -Wextra -Wunused-result -Iinclude -O3 -std=c++98
DFLAGS :=
DEPFLAGS := -MM -MG
LDFLAGS :=

$(BUILD_DIRS)/deps/%.d: $(SRC_DIRS)/%.cpp
	@mkdir -p $(dir $@)
	$(CC) $^ $(CFLAGS) $(DEPFLAGS) -MT $(patsubst $(SRC_DIRS)/%.cpp,$(BUILD_DIRS)/objs/%.o,$<) -o $@

$(BUILD_DIRS)/objs/%.o: $(SRC_DIRS)/%.cpp
	@mkdir -p $(dir $@)
	$(CC) -o $@ $(CFLAGS) -c $<

# **************************************************************************** #

# RULES #

$(NAME): $(OBJS)
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $^

all: $(NAME)

clean:
	@$(RM) -r build/

fclean: clean
	@$(RM) $(NAME)

re:
	@$(MAKE) fclean
	@$(MAKE) all

tidy:
	clang-tidy $(SRCS) -checks=* -- -std=c++98

format:
	clang-format -style=file $(SRCS)

.phony: all clean fclean re tidy format

-include $(DEPS)
