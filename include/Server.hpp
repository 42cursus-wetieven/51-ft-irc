#pragma once

#include "Channel.hpp"
#include "Client.hpp"
#include "utils.hpp"

#include <string>
#include <vector>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/socket.h>

#define MAX_EVENTS 10

#define BUFFER_LEN 1000

class Server {
public:
    /**
     * @brief Constructors
     */
    Server(char portString[], std::string password);
    ~Server(void);

private:
    /**
     * @brief Connection attributes
     */
    int _fdSocket;
    const int _port;
    const std::string _password;
    struct sockaddr_in _address;
    const std::string _userOperator;
    const std::string _passwordOperator;

    /**
     * @brief Epoll attributes
     */
    int _epollFd;
    int _fdCount;
    struct epoll_event _ev;
    struct epoll_event _events[MAX_EVENTS];

    /**
     * @brief Keywords enum for users commands
     */
    enum keyword_values {
        KEYWORD_PASS,
        KEYWORD_NICK,
        KEYWORD_USER,
        KEYWORD_PRIVMSG,
        KEYWORD_JOIN,
        KEYWORD_PART,
        KEYWORD_MODE,
        KEYWORD_QUIT,
        KEYWORD_INVITE,
        KEYWORD_KICK,
        KEYWORD_TOPIC,
        KEYWOR_OPER,
        KEYWORD_KILL,
        KEYWORD_PING,
        KEYWORD_NOTICE,
    } _keywordValue;

    /**
     * @brief Clients vector
     */
    std::vector<Client*> _clients;
    std::vector<Channel*> _channels;
    uint _channelsIds;

private:
    /**
     * @brief Parser
     */
    Result ParseMessage(const std::string message, Client* client);
    Result IdentifyKeywordIn(const std::string message);
    const std::vector<std::string> ExtractArgsFrom(const std::string message) const;
    const std::vector<std::string> ExtractValuesBetweenCommas(const std::string arg) const;

    Result ParsePassWith(const std::vector<std::string> args, Client* client);
    Result ParseNickWith(const std::vector<std::string> args, Client* client);
    Result ParseUserWith(const std::vector<std::string> args, Client* client);

    Result ParsePrivmsgWith(const std::vector<std::string> args, Client* client);
    Result ParseNoticeWith(const std::vector<std::string> args, Client* client);
    Result ParseJoinWith(const std::vector<std::string> args, Client* client);
    Result ParsePartWith(const std::vector<std::string> args, Client* client);

    Result ParsePlusO(Channel* channel, const std::string user_name, Client* client);
    Result ParseMoinsO(Channel* channel, const std::string user_name, Client* client);
    Result ParseModeWith(const std::vector<std::string> args, Client* client);

    Result ParseQuitWith(const std::vector<std::string> args, Client* client);

    Result ParseInviteWith(const std::vector<std::string> args, Client* client);

    Result ParseKickWith(const std::vector<std::string> args, Client* client);
    Result ParseTopicWith(const std::vector<std::string> args, Client* client);

    Result ParseOperWith(const std::vector<std::string> args, Client* client);

    Result ParseKillWith(const std::vector<std::string> args, Client* client);

    Result ParsePingWith(const std::vector<std::string> args, Client* client);

    Result ParseNamesWith(const std::vector<std::string> args, Client* client);

    void SendRegisteredReply(Client* client);

public:
    /**
     * @brief Server accessors
     */
    int getFdSocket(void) const;

    /**
     * @brief Client manager
     */
    Result SearchClientsFromNickname(std::vector<std::string> neededClients, std::vector<Client*>& result, Client* sender, const std::string message);
    Result SearchClientsFromNickname_bis(std::vector<std::string> neededClients, std::vector<Client*>& result, Client* sender, const std::string message);
    void SendMessageTo(Client* client, const std::string message);
    void SendMessageTo(std::vector<Client*> recipients, const std::string message);
    Client* SearchClientFromFdIn(std::vector<Client*> clients, int fd);
    void AddClient(void);
    int ReceiveClient(Client* client, char* buffer);
    bool IsCorrectClientNickname(const std::string name);
    void ClientDestroy(Client* client, const std::string reason);
    Client* SearchClientFrom(const std::string name, const std::vector<Client*> clients);
    void SendMessageFrom(std::vector<Client*>& result, Client* value, Client* sender, const std::string recipient, const std::string message);

    /**
     * @brief Channel manager
     */
    Result JoinChannelsFrom(std::vector<std::string> neededChannels, Client* client);
    Channel* SearchChannelFrom(const std::string name, const std::vector<Channel*> channels);
    void AddChannel(const std::string name, Client* client);
    bool IsCorrectChannelName(const std::string name);
    void ClientLeave(Channel* channel, Client* client);
    Result PartClientFromChannels(std::vector<std::string> neededChannels, Client* client);
    Channel* SearchChannelFrom(const uint id, const std::vector<Channel*> channels);
    std::string GetStringOfConnectedClientsInThis(Channel* channel);

    /**
     * @brief Server deamon
     */
    void run(void);
    void Destroy(void);
};
