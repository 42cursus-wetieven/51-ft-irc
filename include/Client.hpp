#pragma once

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <vector>

/**
 * @brief Set constructor.
 *
 * @param src         gray vertical image.
 */

class Client {
    /**
     * @brief Constructors
     */
public:
    Client(void);
    Client(const int fdSocket);
    ~Client(void);

private:
    /**
     * @brief Socket data attributes
     */
    const int _fdSocket;
    int _clientSocket;
    struct sockaddr_in _clientAddress;
    uint _addrLen;
    char* _ip;
    std::string _buffer;

private:
    /**
     * @brief Client registration attributes
     */
    bool _isPasswordOk;
    bool _isRegistered;
    bool _isUserOk;
    bool _isNicknameOk;
    std::string _nickname;
    struct client_user {
        std::string username;
        std::string host;
        std::string serverName;
        std::string realName;
    } _user;
    bool _isServerOperator;

    std::vector<uint> _joinedChannelsIds;

public:
    /**
     * @brief Socket data accessors
     */
    void SetClientSocket(int clientSocket);
    void SetIpFrom_clientAddress(void);
    void ConcatenateBufferWith(const std::string buffer);

    int GetClientSocket(void) const;
    struct sockaddr_in* GetClientAddressAddr(void);
    uint* GetAddrLenAddr(void);
    char* GetIp(void);
    bool DoTheBufferContainACompleteLine(void);
    const std::string GetAndClearTheBuffer(void);
    const std::string GetBuffer(void);

    /**
     * @brief Client registration accessors
     */
    void SetIsPasswordOk(const bool set);
    void SetIsNicknameOk(const bool set);
    void SetIsUserOk(const bool set);
    bool CheckIfClientIsRegistered(void);
    void SetNickname(const std::string nickname);
    void SetUser(const std::string& username, const std::string& host, const std::string& serverName, const std::string& realName);
    void SetIsServerOperator(const bool set);

    bool GetIsPasswordOk(void) const;
    bool GetIsRegistered(void) const;
    const std::string GetNickname(void) const;
    bool GetIsServerOperator(void) const;

    /**
     * @brief Channels
     */
    void AddJoinedChannelBy(const uint id);
    void DeleteChannelBy(const uint id);
    std::vector<uint> GetJoinedChannelsIds(void);
};
