#pragma once

#define SERVERNAME "God_himself"
#define VERSION "1.0"
#define CREATED "10/05/2022 04:20"
#define SERVERNAMEHEAD (static_cast<std::string>(":") + static_cast<std::string>(SERVERNAME))

#define RPL_WELCOME(nickname) (SERVERNAMEHEAD + " 001 " + nickname + " welcome to this awesome IRC server !" + "\r\n")
#define RPL_PONG(arg) ("PONG " + arg + "\r\n")
#define RPL_YOURHOST (SERVERNAMEHEAD + " 002 " + "Your host is " + SERVERNAME + " running version " + VERSION + "\r\n")
#define RPL_CREATED (SERVERNAMEHEAD + " 003 " + "This server was created " + CREATED + "\r\n")
#define RPL_MYINFO(nickname) (SERVERNAMEHEAD + " 004 " + nickname + " " + SERVERNAME + " " + VERSION + " none " + "none." + "\r\n")
#define RPL_NICK(nickname, new_nick) (":" + nickname + " NICK :" + new_nick + "\r\n")
#define RPL_QUIT(nickname, client) (":" + nickname + " QUIT :" + client + "\r\n")
#define RPL_KICK(nickname, channel, nickname_kick) (":" + nickname + " KICK " + channel + " " + nickname_kick + "\r\n")
#define RPL_KICK_WITH_REASON(nickname, channel, nickname_kick, reason) (":" + nickname + " KICK " + channel + " " + nickname_kick + " :" + reason + "\r\n")

#define RPL_AWAY(nickname, recipient, message) (":" + nickname + " PRIVMSG " + recipient + " :" + message + "\r\n")
#define RPL_NOTOPIC(nickname, channel) (":" + nickname + " TOPIC " + channel + " :" + "No topic on this channel" + "\r\n")
#define RPL_TOPIC(nickname, channel, topic) (":" + nickname + " TOPIC " + channel + " :" + topic + "\r\n")
#define RPL_JOIN(nickname, channel) (":" + nickname + " JOIN " + channel + "\r\n")
#define RPL_PART(nickname, channel) (":" + nickname + " PART " + channel + "\r\n")
#define RPL_YOUREOPER(nickname) (":" + nickname + " OPER " + " :You are now oper" + "\r\n")
#define RPL_NAMREPLY(nickname, channel, nickname_join) (SERVERNAMEHEAD + " 353 " + nickname + " @ " + channel + " :" + nickname_join + "\r\n")
#define RPL_ENDOFNAMES(nickname, channel) (SERVERNAMEHEAD + " 366 " + nickname + " " + channel + " :End of /NAMES list." + "\r\n")
#define RPL_INVITING(nickname, invited, channel) (":" + nickname + " 341 " + invited + " " + channel + "\r\n")
#define RPL_INVITED(nickname, invited, channel) (":" + nickname + " INVITE " + invited + " " + channel + "\r\n")

#define ERR_NOSUCHNICK(nick) (SERVERNAMEHEAD + " 401 " + nick + " :Nickname not found" + "\r\n")
#define ERR_NOSUCHCHANNEL(channel) (SERVERNAMEHEAD + " 403 " + channel + " :Channel not found" + "\r\n")
#define ERR_NORECIPIENT (SERVERNAMEHEAD + " 411 " + ":No recipient given" + "\r\n")
#define ERR_NOTEXTTOSEND (SERVERNAMEHEAD + " 412 " + ":No text to send" + "\r\n")
#define ERR_NONICKNAMEGIVEN (SERVERNAMEHEAD + " 431 " + ":No nickname given" + "\r\n")
#define ERR_ERRONEUSNICKNAME(nick) (SERVERNAMEHEAD + " 432 " + nick + " :Nickname is wrong" + "\r\n")
#define ERR_NICKNAMEINUSE(nick) (SERVERNAMEHEAD + " 433 " + nick + " :Nickname is already in use" + "\r\n")
#define ERR_NOTONCHANNEL(channel) (SERVERNAMEHEAD + " 442 " + channel + " :Not on channel" + "\r\n")
#define ERR_USERONCHANNEL(nick, channel) (SERVERNAMEHEAD + " 443 " + nick + " " + channel + " :User already on channel" + "\r\n")
#define ERR_NEEDMOREPARAMS(command) (SERVERNAMEHEAD + " 461 " + command " :Not enough parameters" + "\r\n")
#define ERR_ALREADYREGISTRED (SERVERNAMEHEAD + " 462 " + ":Already registered" + "\r\n")
#define ERR_PASSWDMISMATCH (SERVERNAMEHEAD + " 464 " + ":Incorrect password !" + "\r\n")
#define ERR_UNKNOWNMODE(char) (SERVERNAMEHEAD + " 472 " + char + " :Unknown mode given" + "\r\n")
#define ERR_INVITEONLYCHAN(channel) (SERVERNAMEHEAD + " 473 " + channel + " :Channel is invite only" + "\r\n")
#define ERR_NOPRIVILEGES (SERVERNAMEHEAD + " 481 " + ":You don't have the necessary privileges" + "\r\n")
#define ERR_CHANOPRIVSNEEDED(channel) (SERVERNAMEHEAD + " 482 " + channel + " :Require special channel privileges" + "\r\n")
#define ERR_NOOPERHOST (SERVERNAMEHEAD + " 491 " + ":You cannot become IRC operator" + "\r\n")

#define ERR_CANNOTSENDTOCHAN(nickname, channel) (SERVERNAMEHEAD + " 404 " + nickname + " " + channel + ":Cannot send to channel\r\n")
